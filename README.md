# Shiftbdr

“La computadora debería estar haciendo el trabajo duro. Para eso fue que se compró, después de todo”. ~Larry Wall.

Esta *webapp* ayudará a detectar qué numeros calificadores han sido detectados como repetidos y cuales numeros has dejado de lado. Esta hecha en Python 2.7 y usando Flask como framework web.
