#!/usr/bin/python3

from flask import Flask, render_template, request
from flask_bootstrap import Bootstrap
from wtforms import Form, TextAreaField, validators

horarios_totales = set(range(1, 58))


class HorariosInput(Form):
    hidden_tag = None
    datos = TextAreaField(label="Lista de horarios",
                          description="Lista donde se presenta el fallo",
                          validators=[validators.DataRequired()])

app = Flask(__name__)
bootstrap = Bootstrap(app)


@app.route("/", methods=["GET", "POST"])
def index():
    form = HorariosInput(request.form)
    if request.method == "GET":
        return render_template("index.html", form=form)
    elif request.method == "POST" and form.validate():
        repetidos, faltantes = revisor(form.datos.data)
        return render_template("results.html",
                               repetidos=repetidos, faltantes=faltantes)


def revisor(datos):
    lista = datos.split("\n")
    registrados = []
    repetidos = []
    horario_presente = ""

    for horario in lista:
        try:
            esnum = int(horario)
            if esnum in registrados:
                repetidos.append([esnum, horario_presente])
            else:
                registrados.append(esnum)
        except ValueError:
            horario_presente = horario

    faltantes = list(horarios_totales - set(registrados))

    return repetidos, faltantes

if __name__ == "__main__":
    app.run()
